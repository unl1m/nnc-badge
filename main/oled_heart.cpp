#include "app.h"

#define TAG "OLED Heart"

#include "nano_engine.h"

//https://github.com/lexus2k/ssd1306/tree/master/examples/direct_draw/move_sprite

/*
 * Define sprite width. The width can be of any size.
 * But sprite height is always assumed to be 8 pixels
 * (number of bits in single byte).
 */
const int spriteWidth = sizeof(heartImage);

/* Declare variable that represents our sprite */
SPRITE sprite;
int speedX = 2;
int speedY = 1;

void setup_heart()
{
    //ssd1306_128x64_i2c_init();
    ssd1306_fillScreen(0x00);
    /* Create sprite at 0,0 position. The function initializes sprite structure. */
    sprite = ssd1306_createSprite( 0, 0, spriteWidth, heartImage );
    /* Draw sprite on the display */
    sprite.draw();
}


void loop_heart()
{
    /* Move sprite every 40 milliseconds */
    //delay(40);
    sprite.x += speedX;
    sprite.y += speedY;
    /* If right boundary is reached, reverse X direction */
    if (sprite.x == (128 - spriteWidth)) speedX = -speedX;
    /* If left boundary is reached, reverse X direction */ 
    if (sprite.x == 0) speedX = -speedX;
    /* Sprite height is always 8 pixels. Reverse Y direction if bottom boundary is reached. */
    if (sprite.y == (64 - 8)) speedY = -speedY;
    /* If top boundary is reached, reverse Y direction */
    if (sprite.y == 0) speedY = -speedY;
    /* Erase sprite on old place. The library knows old position of the sprite. */
    sprite.eraseTrace();
    /* Draw sprite on new place */
    sprite.draw();
}

//typedef NanoEngine<TILE_128x64_MONO> NanoEngine128x64;
//static NanoEngine128x64 engine;
//static NanoSprite<NanoEngine128x64, engine> sprite( {0,0}, {8,8}, heartImage);

void oled_heart_task(void *pvParameters) 
{
  ESP_LOGD(__FUNCTION__, "Starting...");
  setup_heart();
  ssd1306_clearScreen();
  ssd1306_drawBuffer(32, 0, 64, 64, nnclogo);

  while (1) {
    loop_heart();
    vTaskDelay(5);
  }
  vTaskDelete(NULL); 
}


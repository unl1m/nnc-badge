#pragma once

#pragma once

#include <driver/gpio.h>
#include <math.h>
#include "mpu.h"


typedef struct {
	float accTotal[3];
	float accLinear[3];
	float gyro[3];
	float angles[3];  // Angle calculated using DMP
} MPU_DATA;

extern TaskHandle_t mpu_task_handle;

void mpu_task(void *pvParameters);

// should be called only once
void mpu_task_init(void);
void mpu_task_finish(void);

//protected by mutex
int get_mpu_data(MPU_DATA* data);

#include "app.h"

#define TAG "OLED SHIP"

#include "nano_engine.h"

//https://github.com/lexus2k/ssd1306/wiki/Using-NanoEngine-for-systems-with-low-resources

/*
 * Define sprite width. The width can be of any size.
 * But sprite height is always assumed to be 8 pixels
 * (number of bits in single byte).
 */

const PROGMEM uint8_t shipImage[8] =
{
    0B10000000,
    0B01111100,
    0B01100000,
    0B01011110,
    0B01011110,
    0B01100000,
    0B01111100,
    0B10000000
};  

const int spriteWidth = sizeof(shipImage);

NanoEngine1 engine;
typedef NanoFixedSprite<NanoEngine1, engine> CSprite;

class CHeart: public CSprite
{
public:
    CHeart()
    : CSprite({0, 0}, {8, 8}, shipImage) 
    { 
    }

    bool isAlive() { return falling; }

    void respawn()
    {
        int idx = random(8);
        /* Set initial position in scaled coordinates */
        scaled_position = { random(ssd1306_displayWidth() * 8), -8 * 8 };
        /* Use some random speed */
        speed = { random(-16, 16), random(4, 12) };
        /* After countdown timer ticks to 0, change X direction */
        moveTo( scaled_position/8 );
        falling = true;
    }

    void move()
    {
        scaled_position += speed;
        moveTo( scaled_position/8 );
        if (y() >= oled_height() )
        {
            falling = false;
        }
    }

private:
    NanoPoint scaled_position;
    NanoPoint speed;
    bool falling = false;
} heart;


bool onDraw()
{
    engine.canvas.clear();
    // engine.canvas.drawBitmap1(32, 0, 64, 64, nnclogo);
    if (heart.isAlive())
    {
      heart.draw();
    }
    return true;
}

void loop_sprite()
{
    if (!engine.nextFrame()) {
      return;
    }
    if (!heart.isAlive())
    {
      heart.respawn();
    }
    heart.move();
    engine.display();
}

void setup_sprite()
{
    engine.setFrameRate( 10 );
    engine.begin();
    engine.drawCallback( onDraw );
    engine.canvas.setMode(CANVAS_MODE_TRANSPARENT);
    engine.refresh();
}


void oled_space_ship_task(void *pvParameters) 
{
  ESP_LOGD(__FUNCTION__, "Starting...");
  //setup_heart_sprite();
  setup_sprite();
  while (1) {
    //loop_heart_sprite();
    loop_sprite();
    vTaskDelay(5);
  }
  vTaskDelete(NULL); 
}


#pragma once

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

#include <stdio.h>
#include <string.h>

#include "log.h"
#include "esp_system.h"
#include "esp_spi_flash.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"

#include "driver/gpio.h"
#include "sdkconfig.h"

#include "badge_config.h"

//tasks
#include "btn.h"
#include "leds.h"
#include "leds_compass.h"
#include "mpu_task.h"
#include "oled.h"
// #include "wifi.h"

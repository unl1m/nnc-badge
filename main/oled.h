#pragma once

#include "driver/gpio.h"
#include "driver/i2c.h"
#include "ssd1306.h"
#include "nano_gfx.h"


extern const uint8_t nnclogo[];
extern const uint8_t rocket1[];
extern const uint8_t rocket2[];
extern const uint8_t rocket3[];
extern const uint8_t rocket4[];
extern const uint8_t rocket5[];
extern const uint8_t rocket6[];
extern const uint8_t owl[];
extern const uint8_t tm_logo[];
extern const PROGMEM uint8_t heartImage[8];
extern const PROGMEM uint8_t blockImage[8];

int oled_width(void);
int oled_height(void);

//to use with xTaskCreate
void oled_logo_task(void *pvParameters);
void oled_snow_task(void *pvParameters);
void oled_heart_task(void *pvParameters);
void oled_heart_sprite_task(void *pvParameters);
void oled_space_ship_task(void *pvParameters);

void oled_ship_task(void *pvParameters);

// should be called only once
void oled_task_init(void);
    
#include "app.h"


#define TAG "OLED Ship"

#include "nano_engine.h"

//https://github.com/lexus2k/ssd1306/tree/master/examples/direct_draw/move_sprite

/*
 * Define sprite width. The width can be of any size.
 * But sprite height is always assumed to be 8 pixels
 * (number of bits in single byte).
 */

const PROGMEM uint8_t AsteroidsImage[8][8] =
{
    {
        0B00111000,
        0B01010100,
        0B10010010,
        0B11111110,
        0B10010010,
        0B01010100,
        0B00111000,
        0B00000000
    },
    {
        0B00010000,
        0B01010100,
        0B00111000,
        0B11101110,
        0B00111000,
        0B01010100,
        0B00010000,
        0B00000000
    },
    {
        0B00111000,
        0B00010000,
        0B10111010,
        0B11101110,
        0B10111010,
        0B00010000,
        0B00111000,
        0B00000000
    },
    {
        0B00011000,
        0B01011010,
        0B00100100,
        0B11011011,
        0B11011011,
        0B00100100,
        0B01011010,
        0B00011000
    },
    {
        0B00010000,
        0B00111000,
        0B01010100,
        0B11101110,
        0B01010100,
        0B00111000,
        0B00010000,
        0B00000000
    },
    {
        0B10000010,
        0B00101000,
        0B01101100,
        0B00010000,
        0B01101100,
        0B00101000,
        0B10000010,
        0B00000000
    },
    {
        0B01000100,
        0B10101010,
        0B01101100,
        0B00010000,
        0B01101100,
        0B10101010,
        0B01000100,
        0B00000000
    },
    {
        0B00101000,
        0B01010100,
        0B10111010,
        0B01101100,
        0B10111010,
        0B01010100,
        0B00101000,
        0B00000000
    },
};

NanoEngine1 engine;
typedef NanoFixedSprite<NanoEngine1, engine> CAsteroids;

class Asteroids: public CAsteroids
{
public:
    Asteroids()
    : CAsteroids({0, 0}, {8, 8}, nullptr) 
    { 
    }

    bool isAlive() { return falling; }

    void bringToLife()
    {
        int snow_idx = random(8);
        setBitmap( AsteroidsImage[snow_idx] );
        /* Set initial position in scaled coordinates */
        scaled_position = { random(ssd1306_displayWidth() * 8), -8 * 8 };
        /* Use some random speed */
        speed = { random(-16, 16), random(4, 12) };
        /* After countdown timer ticks to 0, change X direction */
        timer = random(24, 48);
        moveTo( scaled_position/8 );
        falling = true;
    }

    void move()
    {
        scaled_position += speed;
       
        timer--;
        if (0 == timer)
        {
            /* Change movement direction */
            speed.x = random(-16, 16);
           
            timer = random(24, 48);
        }
        moveTo( scaled_position/8 );
        if (y() >= static_cast<lcdint_t>(ssd1306_displayHeight()) )
        {
            falling = false;
        }
        // printf("Asteroid X: %i, Y: %i \n", speed.x, speed.y);
    }

private:
    NanoPoint scaled_position;
    NanoPoint speed;
    uint8_t timer;
    bool falling = false;
};

static const uint8_t maxCount = 2;

// These are our snow flakes
Asteroids asteroids[maxCount];

bool onDraw()
{
    engine.canvas.clear();
    
    for (uint8_t i=0; i<maxCount; i++)
    {
        if (asteroids[i].isAlive())
        {
            asteroids[i].draw();
        }
    }
    return true;
}

void setup_asteroids()
{
    engine.setFrameRate( 10 );
    engine.begin();
    engine.drawCallback( onDraw );

    engine.canvas.setMode(CANVAS_MODE_TRANSPARENT);
    engine.refresh();
}

void addAsteroids()
{
    for (uint8_t i=0; i<maxCount; i++)
    {
        if (!asteroids[i].isAlive())
        {
            asteroids[i].bringToLife();
            break;
        }
    }
}


void moveAsteroids()
{
    for (uint8_t i=0; i<maxCount; i++)
    {
        if (asteroids[i].isAlive())
        {
            asteroids[i].move();
        }
    }
}


static uint8_t globalTimer=3;

void loop_asteroids()
{
    if (!engine.nextFrame()) {
      return;
    }
    if (0 == (--globalTimer))
    {
        // Try to add new Asteroids every ~ 90ms 
        globalTimer = 3;
        addAsteroids();
    }
    moveAsteroids();
    engine.display();
}
const PROGMEM uint8_t shipImage[8] =
{
    0B10000000,
    0B01111100,
    0B01100000,
    0B01011110,
    0B01011110,
    0B01100000,
    0B01111100,
    0B10000000
};  
const int spriteWidth = sizeof(shipImage);

/* Declare variable that represents our sprite */
SPRITE sprite;
int speedX = 2;
int speedY = 1;

void setup_ship()
{
    //ssd1306_128x64_i2c_init();
    ssd1306_fillScreen(0x00);
    /* Create sprite at 0,0 position. The function initializes sprite structure. */
    sprite = ssd1306_createSprite( 0, 0, spriteWidth, shipImage );
    /* Draw sprite on the display */
    sprite.draw();
}


void loop_ship()
{
  MPU_DATA data;
    /* Move sprite every 40 milliseconds */
    //delay(40);
    /* If right boundary is reached, reverse X direction */
   
    sprite.x = 64;
    sprite.y = 32;
    get_mpu_data(&data);
    sprite.x -= data.angles[1];
    sprite.y += data.angles[0];

    sprite.eraseTrace();
    /* Draw sprite on new place */
    sprite.draw();
}

void setup_screen_saver(){
  ssd1306_clearScreen();
  ssd1306_drawBuffer(32, 0, 64, 64, rocket1);
  vTaskDelay(20);
  ssd1306_drawBuffer(32, 0, 64, 64, rocket2);
  vTaskDelay(20);
  ssd1306_drawBuffer(32, 0, 64, 64, rocket3);
  vTaskDelay(20);
  ssd1306_drawBuffer(32, 0, 64, 64, rocket4);
  vTaskDelay(20);
  ssd1306_drawBuffer(32, 0, 64, 64, rocket5);
  vTaskDelay(20);
  ssd1306_drawBuffer(32, 0, 64, 64, rocket6);
}

void oled_ship_task(void *pvParameters) 
{
  ESP_LOGD(__FUNCTION__, "Starting...");
  setup_screen_saver();  
  vTaskDelay(250);
  ssd1306_clearScreen();
  setup_ship();
  ssd1306_clearScreen();
  setup_asteroids();

  while (1) {
    loop_ship();
    loop_asteroids();
    vTaskDelay(5);
  }
  vTaskDelete(NULL); 
}


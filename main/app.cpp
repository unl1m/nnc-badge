#include "app.h"

#include "btn.h"

static const char *TAG = __FILE__;



void self_test(void *pvParameters) {
  LOG_FUNC_BEGIN(TAG)
  const int delay1 = 10;
  const int delay2 = 50; 
  const int LED_OFF = 1;
  const int LED_ON = 0;

  gpio_set_level(PIN_LED_BLUE, LED_OFF);
  while (1) {
    if (touchpad_get_state()==TOUCHPAD_STATE_OFF)
    {
      vTaskDelay(delay2); //must do task delay to not block threads
      continue;
    }
    gpio_set_level(PIN_LED_BLUE, LED_ON);
    vTaskDelay(delay1);
    gpio_set_level(PIN_LED_BLUE, LED_OFF);
    vTaskDelay(delay1);
    gpio_set_level(PIN_LED_BLUE, LED_ON);
    vTaskDelay(delay1);
    gpio_set_level(PIN_LED_BLUE, LED_OFF);
    vTaskDelay(delay2);
  }
  LOG_FUNC_END(TAG)
  vTaskDelete(NULL);
}

extern "C" {
    void app_main(void);
}

void app_main() 
{
//  esp_log_level_set("*", ESP_LOG_NONE);
  esp_log_level_set("*", ESP_LOG_DEBUG);
  

  printf(LOG_COLOR_W
         "\n\n\nNoNameBadge created by TechMaker https://techmaker.ua/\r\n"
         "NNC Badge - Example 0: LEDs\r\n"
         );

  /* Print chip information */
  esp_chip_info_t chip_info;
  esp_chip_info(&chip_info);
  printf("This is ESP32 chip with %d CPU cores, WiFi%s%s, ",
          chip_info.cores,
          (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
          (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

  printf("silicon revision %d, ", chip_info.revision);

  printf("%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
          (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");


  //init board GPIO
  gpio_init(); // all gpio init staff here - to avoid conflicts between models like MPU, OLED etc

  //tasks init - variables etc
  leds_task_init();
  touchpad_task_init();
  mpu_task_init();  //i2c init moved to gpio_init
  oled_task_init(); //i2c init moved to gpio_init

  //not migrated yet:
  //wifi_event_group = xEventGroupCreate();
  //xTaskCreatePinnedToCore(&taskBadgeOS, "BadgeOS Shell", 1024 * 6, NULL, 5, NULL, 1);
  //xTaskCreate(&taskLEDsnBTN, "LED n BTN", 1024 * 4, NULL, 5, NULL);
  // xTaskCreatePinnedToCore(&wifi_task, "WiFi", 1024 * 8, NULL, 5, NULL, 0);
  //xTaskCreate(&taskBUZZER, "Buzzer", 1024 * 4, NULL, 5, NULL);
  
  //Touchpad: reads input
  xTaskCreate(&touchpad_task, "Touchpad", 1024 * 4, NULL, 5, NULL);

  //MPU: reads sensora
	 xTaskCreate(&mpu_task , "MPU" , 4096, NULL, 5, &mpu_task_handle);

  //Main blue LED" hearbeat on touchpad pressed
  xTaskCreate(&self_test, "SelfTest", 1024 * 2, NULL, 5, NULL);
  
  //LEDS: choose one of:
  // xTaskCreate(&leds_task, "LEDs n ROLL", 1024 * 4, NULL, 5, NULL);
  //  xTaskCreate(&leds_compass_task, "LEDs compass", 1024 * 4, NULL, 5, NULL);
  
  //OLED, choose one of:
  //xTaskCreate(&oled_logo_task, "OLED", 1024 * 4, NULL, 5, NULL);
  // xTaskCreate(&oled_snow_task, "OLED Snow", 4*1024, NULL, 5, NULL);
  // xTaskCreate(&oled_heart_sprite_task, "OLED Heart sprite", 4*1024, NULL, 5, NULL);
  // xTaskCreate(&oled_space_ship_task, "OLED Space Ship", 4*1024, NULL, 5, NULL);
  xTaskCreate(&oled_ship_task, "OLED Ship", 4*1024, NULL, 5, NULL);
}

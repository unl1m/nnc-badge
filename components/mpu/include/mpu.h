/**
 *	TechMaker
 *	https://techmaker.ua
 *
 *	ESP32 driver for MPU library for MPU6050/6500/9150/9250 with ESP-IDF
 *	based on Invensense eMPL library
 *	13 Sep 2019 by Alexander Olenyev <sasha@techmaker.ua>
 *
 *	Changelog:
 *		- v1.0 initial release for ESP32 ESP-IDF using I2C interface (SPI not yet supported)
 */

#ifndef __MPU_H
#define __MPU_H

#include "inv_mpu.h"
#include "inv_mpu_dmp_motion_driver.h"
#include "mpu_drv.h"
#include "ml_math_func.h"


#define EMPL_TARGET_ESP32

// #define MPU6050
//#define MPU6500
//#define MPU9150
#define MPU9250

#define LOG_ENABLED

#endif /* __MPU_H */

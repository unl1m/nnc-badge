/**
 *	TechMaker
 *	https://techmaker.ua
 *
 *	ESP32 driver for MPU library for MPU6050/6500/9150/9250 with ESP-IDF
 *	based on Invensense eMPL library
 *	13 Sep 2019 by Alexander Olenyev <sasha@techmaker.ua>
 *
 *	Changelog:
 *		- v1.0 initial release for ESP32 ESP-IDF using I2C interface (SPI not yet supported)
 */

#include "mpu_drv.h"

#define ACK_CHECK_EN 0x1   // I2C master will check ack from slave
#define ACK_CHECK_DIS 0x0  // I2C master will not check ack from slave
#define ACK 0x0			   // I2C ack value
#define NACK 0x1		   // I2C nack value

static i2c_port_t i2c_port;

void mpu_i2c_set(i2c_port_t port) {
	i2c_port = port;
}

void mpu_i2c_init(i2c_port_t port, gpio_num_t scl_io_num, gpio_num_t sda_io_num, uint32_t clk_speed) {
	i2c_config_t conf = {
		.mode = I2C_MODE_MASTER,
		.scl_io_num = scl_io_num,
		.scl_pullup_en = GPIO_PULLUP_ENABLE,
		.sda_io_num = sda_io_num,
		.sda_pullup_en = GPIO_PULLUP_ENABLE,
		.master.clk_speed = clk_speed,
	};
	i2c_port = port;
	i2c_param_config(i2c_port, &conf);
	i2c_driver_install(i2c_port, conf.mode, 0, 0, 0);
}

/**
 * @brief I2C write to register function
 *
 * ___________________________________________________________________________________
 * | start | slave_addr + wr_bit + ack | reg_addr + ack | write n bytes + ack | stop |
 * --------|---------------------------|----------------|---------------------|------|
 *
 */
int mpu_i2c_write(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char const* data) {
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (slave_addr << 1) | I2C_MASTER_WRITE, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, reg_addr, ACK_CHECK_EN);
	i2c_master_write(cmd, (uint8_t*)data, length, ACK_CHECK_EN);
	i2c_master_stop(cmd);
	esp_err_t ret = i2c_master_cmd_begin(i2c_port, cmd, 100 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
	return ret;
}

/**
 * @brief I2C read from register function
 *
 * _____________________________________________________________________________________________________________________________________________
 * | start | slave_addr + wr_bit + ack | reg_addr + ack | start | slave_addr + rd_bit + ack | read n-1 bytes + ack |
 * read 1 byte + nack | stop |
 * --------|---------------------------|----------------|-------|---------------------------|----------------------|--------------------|------|
 *
 */

int mpu_i2c_read(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char* data) {
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (slave_addr << 1) | I2C_MASTER_WRITE, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, reg_addr, ACK_CHECK_EN);
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (slave_addr << 1) | I2C_MASTER_READ, ACK_CHECK_EN);
	if (length > 1) {
		i2c_master_read(cmd, data, length - 1, ACK);
	}
	i2c_master_read_byte(cmd, data + length - 1, NACK);
	i2c_master_stop(cmd);
	esp_err_t ret = i2c_master_cmd_begin(i2c_port, cmd, 100 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
	return ret;
}

/**
 * @brief Intialize interrupt routine
 *
 */

int mpu_reg_int_cb(void (*cb)(void*), unsigned short pin, unsigned char active_low) {
	esp_err_t ret = ESP_OK;
	if (!GPIO_IS_VALID_GPIO(pin)) return ESP_ERR_INVALID_ARG;
	gpio_config_t io_conf;
	// interrupt of rising edge if active high, on falling edge if active low
	io_conf.intr_type = active_low == 1 ? GPIO_PIN_INTR_NEGEDGE : GPIO_PIN_INTR_POSEDGE;
	// bit mask of the pins
	io_conf.pin_bit_mask = 1 << pin;
	// set as input mode
	io_conf.mode = GPIO_MODE_INPUT;
	// set pull-down mode if active high
	io_conf.pull_down_en = active_low ? GPIO_PULLDOWN_DISABLE : GPIO_PULLDOWN_ENABLE;
	// set pull-up mode if active low
	io_conf.pull_up_en = active_low ? GPIO_PULLUP_ENABLE : GPIO_PULLUP_DISABLE;
	ret += gpio_config(&io_conf);
	// install gpio isr service
	ret += gpio_install_isr_service(ESP_INTR_FLAG_EDGE | ESP_INTR_FLAG_IRAM);
	// hook isr handler for specific gpio pin
	ret += gpio_isr_handler_add(pin, cb, (void*)0);
	return ret;
}
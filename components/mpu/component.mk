#
# Component Makefile
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)
#
# Include current and eMPL subdirectory
#
COMPONENT_ADD_INCLUDEDIRS := . include eMPL
COMPONENT_SRCDIRS := . eMPL